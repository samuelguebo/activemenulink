<?php
/*
Function for highlighting current page link
*/
// echo "getCurrentPage() is :"." ".getCurrentPage();
function getCurrentPage() {
	$request_path = $_SERVER['REQUEST_URI'];
	$path = explode("/", $request_path); // splitting the path
	$last = end($path);
	// $last = prev($path);
	return $last;
}
function isCurrentPage($page) {
	$result ="";
	$currentPage = getCurrentPage();
	
	if(""!=$currentPage) {
		if ($currentPage ==$page)
		$result ='class="current"';
	}
	echo $result;
}

?>
<div id="templatemo_menu">
                <ul>
                    <li>
                        <a href="index.php" <?php isCurrentPage("index.php");?>>Home</a>
                    </li>

                    <li>
                        <a href="codetic.php" <?php isCurrentPage("services.php");?>>Services</a>
                    </li>

                    <li>
                        <a href="cooperatives.php" <?php isCurrentPage("contact.php");?>>Contact</a>
                    </li>

                </ul>
            </div>